import { NgModule } from '@angular/core';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ObservableComponent } from './observable/observable.component';



@NgModule({
  declarations: [
    AboutComponent,
    ContactComponent,
    DashboardComponent,
    HomeComponent,
    NotFoundComponent,
    ObservableComponent,
  ],
  imports: [
    
  ],
  exports: [
    AboutComponent,
    ContactComponent,
    DashboardComponent,
    HomeComponent,
    NotFoundComponent,
    ObservableComponent,
  ]
})
export class PagesModule { }

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})
export class ObservableComponent implements OnInit {

  constructor() {
    this.getObservable().pipe(
      retry(3)
    ).subscribe(
      valor => console.log("Subscripcion:",valor),
      error => console.warn("Ocurrio un error"),
      () => console.info("Termino la suscripcion")
    )
  }

  ngOnInit(): void {
  }

  getObservable(): Observable<number>{
    let i = -1;
    const obs$ = new Observable<number>(observer => {
      const intervalo = setInterval(() => {
        //console.log("tick");
        i++;
        observer.next(i);
        if(i==10){
          clearInterval(intervalo);
          observer.complete();
        }
        if(i==2){
          //console.log("Error en suscripcion 2");
          i=0;
          observer.error();
        }
      }, 1000);
    });
    return obs$;
  }

}

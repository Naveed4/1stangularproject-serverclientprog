import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-promise',
  templateUrl: './promise.component.html',
  styleUrls: ['./promise.component.css']
})
export class PromiseComponent implements OnInit {

  usuarios: any;

  constructor() { }

  ngOnInit(): void {
    /*const promise = new Promise((resolve, reject) => {
      console.log("esta es una nueva promesa xd");

      if(false){
        resolve("Esta es una nueva promesa xd");
      }else{
        reject("Ocurrio un error");
      }


    });
    
    promise.then((data) => {
      console.log(data);
    });
    
    promise.catch((data) => {
      console.log(data);
    });
    console.log("Fin del evento ngoninit");
   */
    this.getUsers().then((datosUsuarios) => {
      this.usuarios = datosUsuarios;
      console.log(this.usuarios);
    });

  }
  
  // Test function
  getUsers(){
    const promise = new Promise((resolve) => {
      fetch("https://fst-heroku-project-cci.herokuapp.com/usuarios")
      .then(res => res.json())
      .then(res => resolve(res))
    });
    return promise;
  }

}
